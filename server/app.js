var express = require('express');
const bodyParser = require('body-parser');
var email = require('./email.js');
var app = express();

app.set('port', (process.env.PORT || 5000));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(express.static('public'));
app.use(email);

app.get('/flynder_win32', function(req, res){
  var file = __dirname + '/public/files/win32.zip';
  res.download(file);
});

app.get('/flynder_win64', function(req, res){
  var file = __dirname + '/public/files/win64.zip';
  res.download(file);
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});