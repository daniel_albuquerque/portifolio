var nodemailer = require('nodemailer');
var router = require('express').Router();

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'daniel.albuquerque1991',
        pass: '8811Dan1838'
    }
});

router.post('/mail', function (req, res) {
    var mailOptions = {
        from: 'daniel.albuquerque1991@gmail.com',
        to: 'daniel.albuquerque1991@gmail.com',
        subject: 'Portifolio email <' + req.body.email + '>',
        text: (function () {
            return req.body.name + '\n' +
                req.body.email + '\n' +
                req.body.phone + '\n\n' +
                req.body.message
        })()
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            res.status(500).send(error);
        } else {
            console.log('Email sent: ' + info.response);
            res.status(200).send();
        }
    });
});

module.exports = router;