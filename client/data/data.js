window.data = {
    projects: [
        {
            title: "Self control",
            path: "selfcontrol",
            discontinued: false,
            type: "Chrome extension",
            image: "img/portfolio/selfcontrol.png",
            description: "Extension for personal control and navigation monitoring on Chrome browser",
            url: [{
                code: "https://chrome.google.com/webstore/detail/selfcontrol/kjilegnfbmijeejnejbbnpmfclaombki",
                decode: "Chrome Web Store"
            }],
            images: [1, 2, 3, 4, 5]
        },
        {
            title: "Pickz - Puzzle game",
            path: "pickz",
            discontinued: false,
            type: "Web application",
            image: "img/portfolio/pickz.png",
            description: "A little puzzle game in which each world has a different characteristic",
            url: [{
                code: "http://pickz.herokuapp.com",
                decode: "Website"
            }],
            images: [1, 2, 3, 4, 5, 6]
        },
        {
            title: "Black Buy",
            path: "blackbuy",
            discontinued: true,
            type: "Chrome extension",
            image: "img/portfolio/blackbuy.png",
            description: "The Black Buy consists in a buyer central that contains functionalities to help saving money (Brazil only)",
            url: [{
                code: "https://chrome.google.com/webstore/detail/black-buy/hhioidhebbbkapnhloldofjgkmfaoimc",
                decode: "Chrome Web Store"
            }],
            images: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        },
        {
            title: "Flynder",
            path: "flynder",
            discontinued: true,
            type: "NWJS Desktop application",
            image: "img/portfolio/flynder.png",
            description: "An desktop application to notify the user when the flight ticket gets lower price,",
            url: [{
                code: "/flynder_win32",
                decode: "Win 32"
            },{
                code: "/flynder_win64",
                decode: "Win 64"
            }],
            images: [1, 2, 3, 4]
        },
        {
            title: "Virtual Sommelier",
            discontinued: false,
            type: "Cognitive web application",
            image: "img/portfolio/cinemabot.png",
            description: "A cognitive application to recommend a nice beer based in Watsom",
            url: [{
                code: "",
                decode: "Comming soon"
            }],
        }
    ],

    setUpindex: function () {
        ++window['index'] || (window['index'] = 1);
        return;
    },

    getindex: function () {
        return window['index'];
    },

    resetindex: function () {
        window['index'] = null;
        return;
    }
}