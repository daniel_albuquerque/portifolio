(function ($) {
    "use strict";

    $(function () {
        $.when(
            $.get("templates/project-item.html"),
            $.get("templates/project-modal.html")
        ).done(function (projectItemTemplate, projectModalTemplate) {
            var compiledTemplate;
            compiledTemplate = Mustache.render(projectItemTemplate[0], window.data);
            $("#project-items").html(compiledTemplate);

            compiledTemplate = Mustache.render(projectModalTemplate[0], window.data);
            $("#project-modals").html(compiledTemplate);

            $('.slider').owlCarousel({
                items: 1,
                margin: 10,
                nav: true,
                navText: ["",""],
                responsiveClass:true,
                responsive:{
                    0:{
                        autoHeight: false,
                        nav:false,
                        autoWidth: true
                    },
                    600:{
                        autoHeight: true,
                        nav:true
                    }
                }
            });
        });

    })


})(jQuery);
